﻿namespace Game.Data
{
    public class Save
    {
        public int isSave { get; set; }
        public int score { get; set; }
        public string tilesMap { get; set; }
    }
}
