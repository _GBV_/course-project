﻿using SQLite;

namespace Game.Data
{
    public class Player
    {
        [PrimaryKey, AutoIncrement]
        public int id { get; set; }
        public string playerName { get; set; }
        public int playerRecord { get; set; }
        public int playerBiggestTile { get; set; }
    }
}
