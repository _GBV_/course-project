﻿using System;
using System.Windows;

namespace Game
{
    public partial class App : Application
    {
        static string databaseName = "GameDataStorage.db";
        static string folderPath = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments);
        public static string databasePath = System.IO.Path.Combine(folderPath, databaseName);
    }
}
