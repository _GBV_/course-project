﻿using System;

namespace Game.Models
{
    class GameBoardModel
    {
        public const int GameBoardSize = 4;
        public const int StartupTileCount = 2;
        
        private MapModel map = new MapModel(GameBoardSize);
        static Random random = new Random();

        public bool isGameOver;
        public int isPlayerWin;
        private bool moved;
        private int score;

        public int Score
        {
            get { return score; }
            set { score = value; }
        }

        public int size
        {
            get { return GameBoardSize; }
        }

        public void Start()
        {
            Score = 0;
            isGameOver = false;
            isPlayerWin = 0;
            for (int x = 0; x < size; x++)
                for (int y = 0; y < size; y++)
                    map.Set(x, y, 0);
            for(int i = 0; i < StartupTileCount; i++)
                AddRandomNumber();
        }

        public bool IsGameOver()
        {
            if (isGameOver)
                return isGameOver;
            for (int x = 0; x < size; x++)
                for (int y = 0; y < size; y++)
                    if (map.Get(x, y) == 0)
                        return false;

            for (int x = 0; x < size; x++)
                for (int y = 0; y < size; y++)
                    if (map.Get(x, y) == map.Get(x + 1, y) ||
                       map.Get(x, y) == map.Get(x, y + 1))
                        return false;

            isGameOver = true;
            return isGameOver;
        }

        public int IsPlayerWin()
        {
            for (int x = 0; x < size; x++)
                for (int y = 0; y < size; y++)
                    if (map.Get(x, y) == 2048)
                    {
                        isPlayerWin = 1;
                        return isPlayerWin;
                    }
            return isPlayerWin;
        }

        public int GetBiggestTile()
        {
            int max = 0;
            for (int x = 0; x < size; x++)
                for (int y = 0; y < size; y++)
                    if (map.Get(x, y) > max)
                        max = map.Get(x, y);
            return max;
        }

        private void AddRandomNumber()
        {
            if (isGameOver) return;
            for(int j = 0; j < 100; j++)
            {
                int x = random.Next(0, map.size);
                int y = random.Next(0, map.size);
                if (map.Get(x, y) == 0)
                {
                    int choiceNumber = random.Next(1, 11);
                    if(choiceNumber <= 9) map.Set(x, y, 2);
                    else map.Set(x, y, 4);
                    return;
                }
            }
        }

        void Lift(int x, int y, int stepx, int stepy)
        {
            if(map.Get (x, y) > 0)
                while(map.Get(x + stepx, y + stepy) == 0)
                {
                    map.Set(x + stepx, y + stepy, map.Get(x, y));
                    map.Set(x, y, 0);
                    x += stepx;
                    y += stepy;
                    moved = true;
                }
        }

        void Join(int x, int y, int stepx, int stepy)
        {
            if (map.Get(x, y) > 0)
                if (map.Get(x + stepx, y + stepy) == map.Get(x, y))
                {
                    Score += map.Get(x, y) * 2;
                    map.Set(x + stepx, y + stepy, map.Get(x, y) * 2);
                    while (map.Get(x - stepx, y - stepy) > 0)
                    {
                        map.Set(x, y, map.Get(x - stepx, y - stepy));
                        x -= stepx;
                        y -= stepy;
                    }
                    map.Set(x, y, 0);
                    moved = true;
                }
            
            if (isPlayerWin == 0)
                IsPlayerWin();
        }

        public void Left()
        {
            moved = false;
            for (int y = 0; y < map.size; y++)
            {
                for (int x = 1; x < map.size; x++)
                    Lift(x, y, -1, 0);
                for (int x = 1; x < map.size; x++)
                    Join(x, y, -1, 0);
            }

            if (moved) AddRandomNumber();
        }

        public void Right()
        {
            moved = false;
            for (int y = 0; y < map.size; y++)
            {
                for (int x = map.size - 2; x >= 0; x--)
                    Lift(x, y, +1, 0);
                for (int x = map.size - 2; x >= 0; x--)
                    Join(x, y, +1, 0);
            }

            if (moved) AddRandomNumber();
        }

        public void Up()
        {
            moved = false;
            for (int x = 0; x < map.size; x++)
            {
                for (int y = 1; y < map.size; y++)
                    Lift(x, y, 0, -1);
                for (int y = 1; y < map.size; y++)
                    Join(x, y, 0, -1);
            }

            if (moved) AddRandomNumber();
        }

        public void Down()
        {
            moved = false;
            for (int x = 0; x < map.size; x++)
            {
                for (int y = map.size - 2; y >= 0; y--)
                    Lift(x, y, 0, +1);
                for (int y = map.size - 2; y >= 0; y--)
                    Join(x, y, 0, +1);
            }

            if (moved) AddRandomNumber();
        }

        public int GetMap(int x, int y)
        {
            return map.Get(x, y);
        }

        public string GetMapString()
        {
            string str = "";
            for (int x = 0; x < size; x++)
                for (int y = 0; y < size; y++)
                    str += $"{ map.Get(x, y)} ";
            return str;
        }

        public void SetMap(string Map, int TestScore)
        {
            Score = TestScore;

            string[] subs = Map.Split(' ');
            int i = 0;
            for (int x = 0; x < size; x++)
                for (int y = 0; y < size; y++)
                    if(i < 16) map.Set(x, y, Convert.ToInt32(subs[i++]));
        }
    }
}
