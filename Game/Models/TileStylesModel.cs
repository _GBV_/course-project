﻿using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;

namespace Game.Models
{
    class TileStylesModel
    {
        private static readonly SolidColorBrush Tile0 = new SolidColorBrush((Color)ColorConverter.ConvertFromString("#CDC1B4"));
        private static readonly SolidColorBrush Tile2 = new SolidColorBrush((Color)ColorConverter.ConvertFromString("#EEE4DA"));
        private static readonly SolidColorBrush Tile4 = new SolidColorBrush((Color)ColorConverter.ConvertFromString("#EDE0C8"));
        private static readonly SolidColorBrush Tile8 = new SolidColorBrush((Color)ColorConverter.ConvertFromString("#F2B179"));
        private static readonly SolidColorBrush Tile16 = new SolidColorBrush((Color)ColorConverter.ConvertFromString("#F59563"));
        private static readonly SolidColorBrush Tile32 = new SolidColorBrush((Color)ColorConverter.ConvertFromString("#F67C5F"));
        private static readonly SolidColorBrush Tile64 = new SolidColorBrush((Color)ColorConverter.ConvertFromString("#F65E3B"));
        private static readonly SolidColorBrush Tile128 = new SolidColorBrush((Color)ColorConverter.ConvertFromString("#EDCF72"));
        private static readonly SolidColorBrush Tile256 = new SolidColorBrush((Color)ColorConverter.ConvertFromString("#EDCC61"));
        private static readonly SolidColorBrush Tile512 = new SolidColorBrush((Color)ColorConverter.ConvertFromString("#EDC850"));
        private static readonly SolidColorBrush Tile1024 = new SolidColorBrush((Color)ColorConverter.ConvertFromString("#EDC53F"));
        private static readonly SolidColorBrush Tile2048 = new SolidColorBrush((Color)ColorConverter.ConvertFromString("#EDC22E"));
        private static readonly SolidColorBrush TileSuper = new SolidColorBrush((Color)ColorConverter.ConvertFromString("#3C3A32"));

        private static readonly SolidColorBrush WhiteText = new SolidColorBrush((Color)ColorConverter.ConvertFromString("#FFFFFF"));
        private static readonly SolidColorBrush BlackText = new SolidColorBrush((Color)ColorConverter.ConvertFromString("#776E65"));

        private static int HugeFont = 45;
        private static int LargeFont = 30;
        private static int MediumFont = 20;
        private static int SmallFont = 10;

        public void SetDesign(Border border, Label label, int number)
        {
            border.CornerRadius = new CornerRadius(10);
            border.BorderBrush = new SolidColorBrush((Color)ColorConverter.ConvertFromString("#3C3A32"));
            border.Margin = new Thickness(5);
            border.Background = GetBackgroundColor(number);
            border.Child = label;

            label.Foreground = GetTextColor(number);
            label.FontSize = GetFontSize(number);
            label.FontWeight = FontWeights.Bold;
            label.HorizontalAlignment = HorizontalAlignment.Center;
            label.VerticalAlignment = VerticalAlignment.Center;
        }

        public SolidColorBrush GetBackgroundColor(int number)
        {
            switch (number)
            {
                case 0: return Tile0;
                case 2: return Tile2;
                case 4: return Tile4;
                case 8: return Tile8;
                case 16: return Tile16;
                case 32: return Tile32;
                case 64: return Tile64;
                case 128: return Tile128;
                case 256: return Tile256;
                case 512: return Tile512;
                case 1024: return Tile1024;
                case 2048: return Tile2048;
                default: return TileSuper;
            }
        }

        public SolidColorBrush GetTextColor(int number)
        {
            switch (number)
            {
                case 0:
                case 2:
                case 4: return BlackText;
                default: return WhiteText;
            }
        }

        public int GetFontSize(int number)
        {
            switch (number)
            {
                case 0:
                case 2:
                case 4:
                case 8:
                case 16:
                case 32:
                case 64: return HugeFont;
                case 128:
                case 256:
                case 512: return LargeFont;
                case 1024:
                case 2048:
                case 4096:
                case 8192: return MediumFont;
                default: return SmallFont;
            }
        }
    }
}
