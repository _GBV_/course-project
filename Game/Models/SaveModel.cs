﻿using SQLite;
using Game.Data;

namespace Game.Models
{
    class SaveModel
    {
        public void SaveGame(SQLiteConnection connection, int isSave, int playerScore, string tilesMapStr)
        {
            try
            {
                connection.Query<Save>("update Save set isSave='" + isSave + "'");
                connection.Query<Save>("update Save set score='" + playerScore + "'");
                connection.Query<Save>("update Save set tilesMap='" + tilesMapStr + "'");
            }
            catch
            {
                Save newSave = new Save()
                {
                    isSave = 1,
                    score = playerScore,
                    tilesMap = tilesMapStr
                };

                try
                {
                    connection.Insert(newSave);
                }
                catch
                {
                    connection.CreateTable<Save>();
                    connection.Insert(newSave);
                }
            }
        }
    }
}
