﻿using SQLite;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using Game.Data;

namespace Game.ViewModels
{
    public partial class GameOverViewModel : Window
    {
        private int Score;
        private int BiggestTile;

        public GameOverViewModel()
        {
            InitializeComponent();
        }

        public GameOverViewModel(int score, int biggestTile)
        {
            InitializeComponent();
            Score = score;
            BiggestTile = biggestTile;
        }

        private void ButtonSend_Click(object sender, RoutedEventArgs e)
        {
            if (PlayerName.Text.Length < 2)
            {
                MessageBox.Show("Minimum nickname length 2 characters");
                return;
            }

            SQLiteConnection connection = new SQLiteConnection(App.databasePath);
            List<Player> players = connection.Table<Player>().ToList();

            foreach (var player in players)
            {
                if (player.playerName == PlayerName.Text)
                {
                    if (player.playerRecord < Score)
                    {
                        connection.Query<Player>("update Player set playerRecord='" + Score + "' where playerName = '" + PlayerName.Text + "'").FirstOrDefault();
                        connection.Query<Player>("update Player set playerBiggestTile='" + BiggestTile + "' where playerName = '" + PlayerName.Text + "'").FirstOrDefault();
                    }

                    Close();
                    return;
                }
            }

            Player newPlayer = new Player()
            {
                playerName = PlayerName.Text,
                playerRecord = Score,
                playerBiggestTile = BiggestTile
            };

            connection.Insert(newPlayer);
            Close();
        }
    }
}
