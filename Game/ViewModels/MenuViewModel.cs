﻿using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Game.Models;
using SQLite;
using Game.Data;

namespace Game.ViewModels
{
    public partial class MenuViewModel : Page
    {
        SQLiteConnection connection = new SQLiteConnection(App.databasePath);
        bool isSave = false;

        public MenuViewModel()
        {
            try
            {
                List<Save> saves = connection.Table<Save>().ToList();
                foreach (var save in saves)
                    if (save.isSave == 1) isSave = true;
                    else isSave = false;
            }
            catch
            {
                isSave = false;
            }

            InitializeComponent();

            if (!isSave)
            {
                ContinueButton.IsEnabled = false;
            }
        }

        private void NewGameButton_Click(object sender, RoutedEventArgs e)
        {
            if (isSave)
            {
                ConfirmViewModel confirmWindow = new ConfirmViewModel("Your previous saves will be lost", 17);
                confirmWindow.ShowDialog();
                if (!confirmWindow.choice) return;
            }

            SaveModel save = new SaveModel();
            save.SaveGame(connection, 0, 0, "0");
            NavigationService.Navigate(new GameViewModel());
        }

        private void ContinueButton_Click(object sender, RoutedEventArgs e)
        {
            GameBoardModel savedGameBoard = new GameBoardModel();
            List<Save> saves = connection.Table<Save>().ToList();
            foreach (var save in saves)
                savedGameBoard.SetMap(save.tilesMap, save.score);

            NavigationService.Navigate(new GameViewModel(savedGameBoard));
        }

        private void StatisticsButton_Click(object sender, RoutedEventArgs e)
        {
            NavigationService.Navigate(new StatisticsViewModel());
        }

        private void ExitButton_Click(object sender, RoutedEventArgs e)
        {
            Environment.Exit(0);
        }
        
        private void InfoButton_Click(object sender, RoutedEventArgs e)
        {
            InfoViewModel infoWindow = new InfoViewModel();
            infoWindow.ShowDialog();
        }
    }
}
