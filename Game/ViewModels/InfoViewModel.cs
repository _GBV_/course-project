﻿using System.Windows;

namespace Game.ViewModels
{
    public partial class InfoViewModel : Window
    {
        public InfoViewModel()
        {
            InitializeComponent();
        }

        private void ButtonOK_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }
    }
}
