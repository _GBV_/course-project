﻿using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Navigation;
using Game.Data;
using Game.Models;
using SQLite;

namespace Game.ViewModels
{
    public partial class GameViewModel : Page
    {
        SQLiteConnection connection = new SQLiteConnection(App.databasePath);
        TileStylesModel tileStyles = new TileStylesModel();
        GameBoardModel gameBoardModel;
        public Label[,] labels;
        public Border[,] borders;

        public GameViewModel()
        {
            PrepareGame();
            gameBoardModel = new GameBoardModel();
            gameBoardModel.Start();
            Show(gameBoardModel);
        }

        internal GameViewModel(GameBoardModel saves)
        {
            PrepareGame();
            gameBoardModel = saves;
            TextBoxScore.Text = $"{gameBoardModel.Score}";
            Show(gameBoardModel);
        }

        void PrepareGame()
        {
            InitializeComponent();
            PrepareGameBoard(GameBoardModel.GameBoardSize);
            PreviewKeyDown += new KeyEventHandler(IsKeyDown);
        }

        private void PrepareGameBoard(int gameBoardSize)
        {
            for (int i = 0; i < gameBoardSize; i++)
                GameBoardGrid.RowDefinitions.Add(new RowDefinition());
            for (int j = 0; j < gameBoardSize; j++)
                GameBoardGrid.ColumnDefinitions.Add(new ColumnDefinition());
        }

        void Show(GameBoardModel model)
        {
            labels = new Label[model.size, model.size];
            borders = new Border[model.size, model.size];
            
            for (int x = 0; x < model.size; x++)
                for (int y = 0; y < model.size; y++)
                {
                    int number = model.GetMap(x, y);

                    if (number != 0) labels[x, y] = new Label { Content = $"{number}" };
                    else labels[x, y] = new Label { Content = "" };
                    borders[x, y] = new Border();

                    tileStyles.SetDesign(borders[x, y], labels[x, y], number);

                    GameBoardGrid.Children.Add(borders[x, y]);
                    Grid.SetColumn(borders[x, y], x);
                    Grid.SetRow(borders[x, y], y);
                    TextBoxScore.Focus();
                }
            
            connection.CreateTable<Player>();
            List<Player> players = connection.Table<Player>().ToList();
            foreach (var player in players)
            {
                if (Convert.ToInt32(TextBoxBest.Text) < player.playerRecord)
                    TextBoxBest.Text = Convert.ToString(player.playerRecord);
            }
        }

        private void IsKeyDown(object sender, KeyEventArgs e)
        {
            if (gameBoardModel.IsGameOver())
            {
                GameOver();
                return;
            }
            switch (e.Key)
            {
                case Key.Left:
                    gameBoardModel.Left();
                    UpdateTiles(gameBoardModel);
                    break;
                case Key.Right:
                    gameBoardModel.Right();
                    UpdateTiles(gameBoardModel);
                    break;
                case Key.Up:
                    gameBoardModel.Up();
                    UpdateTiles(gameBoardModel);
                    break;
                case Key.Down:
                    gameBoardModel.Down();
                    UpdateTiles(gameBoardModel);
                    break;
                default:
                    break;
            }
            if (gameBoardModel.isPlayerWin == 1)
            {
                YouWinViewModel youWinWindow = new YouWinViewModel();
                youWinWindow.ShowDialog();
                gameBoardModel.isPlayerWin = 2;
            }
        }

        void UpdateTiles(GameBoardModel model)
        {
            for (int x = 0; x < model.size; x++)
                for (int y = 0; y < model.size; y++)
                {
                    int number = model.GetMap(x, y);

                    if (number != 0) labels[x, y].Content = $"{number}";
                    else labels[x, y].Content = "";
                    
                    borders[x, y].Background = tileStyles.GetBackgroundColor(number);
                    labels[x, y].Foreground = tileStyles.GetTextColor(number);
                    labels[x, y].FontSize = tileStyles.GetFontSize(number);

                    TextBoxScore.Text = $"{model.Score}";
                }
        }

        private void MenuButton_Click(object sender, RoutedEventArgs e)
        {
            if (Convert.ToInt32(TextBoxScore.Text) > 0)
            {
                SaveModel save = new SaveModel();
                save.SaveGame(connection, 1, Convert.ToInt32(TextBoxScore.Text), gameBoardModel.GetMapString());
            }

            NavigationService.Navigate(new MenuViewModel());
        }

        private void RestartButton_Click(object sender, RoutedEventArgs e)
        {
            if (Convert.ToInt32(TextBoxScore.Text) > 0)
            {
                ConfirmViewModel confirmWindow = new ConfirmViewModel("This result will be lost", 17);
                confirmWindow.ShowDialog();

                if (!confirmWindow.choice) return;
            }

            RestartGame();
        }

        private void GameOver()
        {
            GameOverViewModel gameOverWindow = new GameOverViewModel(Convert.ToInt32(TextBoxScore.Text), gameBoardModel.GetBiggestTile());
            gameOverWindow.ShowDialog();
            RestartGame();
        }

        private void RestartGame()
        {
            SaveModel save = new SaveModel();
            save.SaveGame(connection, 0, 0, "0");
            gameBoardModel.Start();
            Show(gameBoardModel);
            TextBoxScore.Text = "0";
        }
    }
}
