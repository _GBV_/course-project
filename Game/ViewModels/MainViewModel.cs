﻿using System.ComponentModel;
using System.Windows;

namespace Game.ViewModels
{
    public partial class MainViewModel : Window
    {
        public MainViewModel()
        {
            InitializeComponent();
            MainWindowFrame.NavigationService.Navigate(new MenuViewModel());
        }

        private void Window_Closing(object sender, CancelEventArgs e)
        {
            ConfirmViewModel confirmWindow = new ConfirmViewModel("If you close this window, the\nunsaved result will be lost. Return\nto the menu and click 'Save and Exit'");
            confirmWindow.ShowDialog();
            if (!confirmWindow.choice) e.Cancel = true;
        }
    }
}
