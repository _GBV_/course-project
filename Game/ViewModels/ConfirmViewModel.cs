﻿using System.Windows;

namespace Game.ViewModels
{
    public partial class ConfirmViewModel : Window
    {
        public bool choice = false;

        public ConfirmViewModel()
        {
            InitializeComponent();
        }

        public ConfirmViewModel(string str, int textSize = 14)
        {
            InitializeComponent();
            TextInfo.FontSize = textSize;
            TextInfo.Content = str;
        }

        private void ButtonYes_Click(object sender, RoutedEventArgs e)
        {
            choice = true;
            Close();
        }

        private void ButtonNo_Click(object sender, RoutedEventArgs e)
        {
            choice = false;
            Close();
        }
    }
}
