﻿using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Game.Data;
using SQLite;

namespace Game.ViewModels
{
    public partial class StatisticsViewModel : Page
    {
        public StatisticsViewModel()
        {
            InitializeComponent();

            SQLiteConnection connection = new SQLiteConnection(App.databasePath);
            connection.CreateTable<Player>();
            List<Player> players = connection.Table<Player>().ToList();
            
            foreach (var player1 in players)
            {
                int i = 1;
                foreach (var player2 in players)
                {
                    if (player1.playerRecord < player2.playerRecord) i++;
                    if (player1.playerRecord == player2.playerRecord && 
                        player1.playerBiggestTile < player2.playerBiggestTile) i++;
                }
                player1.id = i;
            }
            dataGrid.ItemsSource = players;
        }

        private void MenuButton_Click(object sender, RoutedEventArgs e)
        {
            NavigationService.Navigate(new MenuViewModel());
        }
    }
}
